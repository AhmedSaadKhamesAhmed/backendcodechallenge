<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class CommitRepository extends ServiceEntityRepository
{
    /**
     *  1 hour cache, could be changed based on how much we run the importer
     */
    const ALL_COMMITS_QUERY_CACHE_LIFETIME = 3600;

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * CommitRepository constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $commitsValues
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function saveCommitsBulk(array $commitsValues): bool
    {
        $conn = $this->entityManager->getConnection();

        $sql = "
            INSERT IGNORE INTO commit (hash,committer_name, url)
            VALUES " . implode(',', $commitsValues);

        $stmt = $conn->prepare($sql);

        $result = $stmt->execute($commitsValues);

        return $result;
    }

    /**
     * @return array
     */
    public function getAllCommits(): array
    {
        $query = $this->entityManager->createQuery(
            'SELECT c
            FROM App\Entity\Commit c
            ORDER BY c.id ASC'
        )->enableResultCache(self::ALL_COMMITS_QUERY_CACHE_LIFETIME);

        return $query->getResult();
    }
}
