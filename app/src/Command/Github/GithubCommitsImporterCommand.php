<?php

declare(strict_types=1);

namespace App\Command\Github;

use App\Service\Github\GithubCommitsImporterManager;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GithubCommitsImporterCommand extends Command
{
    use LoggerAwareTrait;

    /**
     * @var GithubCommitsImporterManager
     */
    public GithubCommitsImporterManager $manager;

    /**
     * @var string
     */
    protected static $defaultName = 'app:github:commits:importer';

    /**
     * GithubCommitsImporterCommand constructor.
     * @param GithubCommitsImporterManager $manager
     * @param LoggerInterface $logger
     */
    public function __construct(
        GithubCommitsImporterManager $manager,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->manager = $manager;
        $this->setLogger($logger);
    }


    protected function configure()
    {
        $this->setDescription('github most recent commits importer!')
            ->setDefinition([
                new InputOption('limit', 'l', InputOption::VALUE_REQUIRED, 'number of most recent commits', 25),
                new InputOption('per_chunk', 'pc', InputOption::VALUE_REQUIRED, 'number of items per chunk', 5)]);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('run github commits importer!');

        $limit = (int) $input->getOption('limit');
        $perChunkLimit = (int) $input->getOption('per_chunk');

        $chunksCount = (int) ceil($limit / $perChunkLimit);
        $io->progressStart($chunksCount);

        for ($chunkKey =  1; $chunkKey <= $chunksCount; $chunkKey++) {
            $this->manager->importLastCommits($perChunkLimit, $chunkKey, $io);
            $io->progressAdvance();
        }

        $io->progressFinish($limit);
        $io->success('successfully imported!');

        return 1;
    }
}
