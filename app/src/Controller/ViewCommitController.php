<?php

namespace App\Controller;

use App\Service\ViewCommitManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ViewCommitController extends AbstractController
{
    /**
     * @var ViewCommitManager
     */
    private ViewCommitManager $manager;

    /**
     * ViewCommitController constructor.
     * @param ViewCommitManager $manager
     */
    public function __construct(ViewCommitManager $manager)
    {
        $this->manager = $manager;
    }


    /**
     * @Route("/commits")
     */
    public function listCommitsAction(): Response
    {
        $commits = $this->manager->getAllCommits();

        return $this->render('commits.html.twig', [
            'commits' => $commits,
        ]);
    }
}
