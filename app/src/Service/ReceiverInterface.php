<?php

declare(strict_types=1);

namespace App\Service;

interface ReceiverInterface
{
    /**
     * @param array $data
     * @param string $dataType
     */
    public function receiveData(array $data, string $dataType): void;
}
