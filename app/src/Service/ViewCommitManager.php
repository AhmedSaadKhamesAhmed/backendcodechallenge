<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Commit;
use Doctrine\ORM\EntityManagerInterface;

class ViewCommitManager
{
    protected EntityManagerInterface $entityManager;

    /**
     * ViewCommitManager constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getAllCommits(): array
    {
        $commits = $this->entityManager->getRepository(Commit::class)->getAllCommits();

        return $commits;
    }
}
