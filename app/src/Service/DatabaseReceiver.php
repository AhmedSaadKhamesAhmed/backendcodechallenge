<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\CommitMapperInterface;
use App\Entity\Commit;
use Doctrine\ORM\EntityManagerInterface;

class DatabaseReceiver implements ReceiverInterface
{
    protected EntityManagerInterface $entityManager;

    protected CommitMapperInterface $mapper;

    /**
     * DatabaseReceiver constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CommitMapperInterface $mapper
    ) {
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }

    /**
     * @param array $data
     * @param string $dataType
     */
    public function receiveData(array $data, string $dataType): void
    {
        if ($dataType === "commits") {
            $this->receiveCommits($data);
        }

        // todo: else do something else for other requirements
    }

    /**
     * @param array $commits
     */
    protected function receiveCommits(array $commits): void
    {
        $commitsInsertedValues = [];
        foreach ($commits as $commit) {
            $mappedCommit = $this->mapper->mapCommitResponse($commit);

            $commitsInsertedValues[] = "('" . $mappedCommit['hash'] .
                "','" . $mappedCommit['commiterName'] .
                "','" . $mappedCommit['url'] .
                "')";
        }

        $this->entityManager->getRepository(Commit::class)->saveCommitsBulk($commitsInsertedValues);
    }
}
