<?php

namespace App\Service;

use Symfony\Component\Console\Style\SymfonyStyle;

interface ClientAdapterInterface
{
    /**
     * @param int $limit
     * @param int $chunkKey
     * @param SymfonyStyle $io
     * @return mixed
     */
    public function getLastRepoCommits(int $limit, int $chunkKey, SymfonyStyle $io);
}
