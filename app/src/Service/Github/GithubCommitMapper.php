<?php

namespace App\Service\Github;

use App\Service\CommitMapperInterface;

class GithubCommitMapper implements CommitMapperInterface
{
    /**
     * @param object $commitResponse
     * @return array
     */
    public function mapCommitResponse(object $commitResponse): array
    {
        $mappedCommit = [
            'hash' => $commitResponse->sha,
            'commiterName' => $commitResponse->commit->committer->name,
            'url' => $commitResponse->html_url
        ];

        return $mappedCommit;
    }
}
