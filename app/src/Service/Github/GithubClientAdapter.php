<?php

declare(strict_types=1);

namespace App\Service\Github;

use App\Service\ClientAdapterInterface;
use Exception;
use Throwable;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GithubClientAdapter implements ClientAdapterInterface
{
    use LoggerAwareTrait;

    protected Client $client;

    private string $token;

    private string $repoName;

    /**
     * GithubClientAdapter constructor.
     * @param Client $client
     * @param string $token
     * @param string $repoName
     */
    public function __construct(
        Client $client,
        string $token,
        string $repoName,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->token = $token;
        $this->repoName = $repoName;
        $this->setLogger($logger);
    }

    /**
     * @param int $limit
     * @param int $chunkKey
     * @param SymfonyStyle|null $io
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLastRepoCommits(int $limit, int $chunkKey, SymfonyStyle $io): array
    {
        $commits = [];
        $baseUri = $this->client->getConfig('base_uri');

        try {
            $response = $this->client->get(
                "$baseUri/repos/$this->repoName/commits",
                [
                    RequestOptions::HEADERS => ['accept' => 'application/vnd.github.v3+json'],
                    RequestOptions::HEADERS => ['Authorization' => 'token ' . $this->token],
                    RequestOptions::QUERY => ['per_page' => $limit , 'page' => $chunkKey]
                ]
            );

            if (200 === $response->getStatusCode()) {
                if ($io) {
                    $io->comment('successfully got the last repo commits for chunk no. ' . $chunkKey);
                }
                $commits = \GuzzleHttp\json_decode($response->getBody()->getContents());
            } else {
                $io->comment('failed to get the last repo commits for chunk no. ' . $chunkKey);
                throw new Exception("failed to get the last repo commits for chunk no. ' . $chunkKey");
            }
        } catch (Throwable $e) {
            $io->error($e->getMessage());
            $this->logger->error($e->getMessage());
        }

        return $commits;
    }
}
