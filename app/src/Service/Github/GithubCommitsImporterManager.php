<?php

namespace App\Service\Github;

use App\Service\ClientAdapterInterface;
use App\Service\ReceiverInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class GithubCommitsImporterManager
{
    use LoggerAwareTrait;

    protected ClientAdapterInterface $clientAdapter;

    protected ReceiverInterface $receiver;

    /**
     * GithubCommitsImporterManager constructor.
     * @param ClientAdapterInterface $clientAdapter
     * @param ReceiverInterface $receiver
     * @param LoggerInterface $logger
     */
    public function __construct(
        ClientAdapterInterface $clientAdapter,
        ReceiverInterface $receiver,
        LoggerInterface $logger
    ) {
        $this->clientAdapter = $clientAdapter;
        $this->receiver = $receiver;
        $this->setLogger($logger);
    }

    /**
     * @param int $limit
     * @param int $chunkKey
     * @param SymfonyStyle $io
     * @return bool
     */
    public function importLastCommits(int $limit, int $chunkKey, SymfonyStyle $io)
    {
        try {
            $commits = $this->clientAdapter->getLastRepoCommits($limit, $chunkKey, $io);

            if (empty($commits)) {
                $io->error('no commits!');
                $this->logger->error('no commits!');
                return false;
            } else {
                $this->receiver->receiveData($commits, "commits");
            }
        } catch (Throwable $e) {
            $io->error($e->getMessage());
            $this->logger->error($e->getMessage());
            return false;
        }

        return true;
    }
}
