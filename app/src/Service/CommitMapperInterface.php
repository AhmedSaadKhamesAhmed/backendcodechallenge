<?php

namespace App\Service;

interface CommitMapperInterface
{
    /**
     * @param object $commitResponse
     * @return array
     */
    public function mapCommitResponse(object $commitResponse): array;
}
