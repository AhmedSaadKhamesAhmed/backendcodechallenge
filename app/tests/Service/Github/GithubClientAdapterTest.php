<?php

namespace App\Tests\Service\Github;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use App\Service\Github\GithubClientAdapter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class GithubClientAdapterTest extends TestCase
{
    private $clientMock;

    private $symfonyStyleMock;

    private $token = 'dummy token';

    private $repoName = 'nodejs/node';

    protected function setUp(): void
    {
        $this->clientMock = $this->createMock(Client::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->symfonyStyleMock = $this->createMock(SymfonyStyle::class);
    }

    protected function tearDown(): void
    {
        $this->clientMock = null;
        $this->loggerMock = null;
        $this->symfonyStyleMock = null;
    }

    public function testGetLastRepoCommitsSuccess()
    {
        $commitResponse = new \stdClass();
        $commitResponse->sha = '3fd04e789e569479de412b0c8a1265233bbb7115';

        $this->clientMock->expects($this->once())
            ->method('get')
            ->willReturn(
                new Response(200, [], '[{"sha" : "3fd04e789e569479de412b0c8a1265233bbb7115"}]')
            );

        $githubClientAdapter = new GithubClientAdapter(
            $this->clientMock,
            $this->token,
            $this->repoName,
            $this->loggerMock
        );

        $response = $githubClientAdapter->getLastRepoCommits(5, 1, $this->symfonyStyleMock);

        $this->assertIsArray($response);
        $this->assertNotEmpty($response);
    }

    /**
     * @expectedException Throwable
     */
    public function testGetLastRepoCommitsApiCallFail()
    {
        $commitResponse = new \stdClass();
        $commitResponse->sha = '3fd04e789e569479de412b0c8a1265233bbb7115';

        $this->clientMock->expects($this->once())
            ->method('get')
            ->willReturn(
                new Response(403, [], '[{"error" : "Forrbiddwn"}]')
            );

        $githubClientAdapter = new GithubClientAdapter(
            $this->clientMock,
            $this->token,
            $this->repoName,
            $this->loggerMock
        );

        $githubClientAdapter->getLastRepoCommits(5, 1, $this->symfonyStyleMock);
    }
}
