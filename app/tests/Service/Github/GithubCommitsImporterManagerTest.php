<?php

namespace App\Tests\Service\Github;

use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use App\Service\ReceiverInterface;
use App\Service\ClientAdapterInterface;
use App\Service\Github\GithubCommitsImporterManager;
use Symfony\Component\Console\Style\SymfonyStyle;

class GithubCommitsImporterManagerTest extends TestCase
{
    private $receiverMock;

    private $clientAdapterMock;

    private $loggerMock;

    protected function setUp(): void
    {
        $this->clientAdapterMock = $this->createMock(ClientAdapterInterface::class);
        $this->receiverMock = $this->createMock(ReceiverInterface::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->symfonyStyleMock = $this->createMock(SymfonyStyle::class);
    }

    protected function tearDown(): void
    {
        $this->clientAdapterMock = null;
        $this->receiverMock = null;
        $this->loggerMock = null;
        $this->symfonyStyleMock = null;
    }

    public function testImportLastCommitsFail()
    {
        // mock empty return from the api
        $this->clientAdapterMock->expects($this->once())
            ->method('getLastRepoCommits');

        $manager = new GithubCommitsImporterManager(
            $this->clientAdapterMock,
            $this->receiverMock,
            $this->loggerMock
        );

        $this->assertFalse($manager->importLastCommits(5, 1, $this->symfonyStyleMock));
    }

    public function testImportLastCommitsSuccess()
    {
        $returnedCommit = new \stdClass();
        $returnedCommit->sha = '3fd04e789e569479de412b0c8a1265233bbb7115';
        $returnedCommit->node_id = 'MDY6Q29tbWl0MjcxOTM3Nzk6M2ZkMDRlNzg5ZTU2OTQ3OWRlNDEyYjBjOGExMjY1MjMzYmJiNzExNQ==';

        $this->clientAdapterMock->expects($this->once())
        ->method('getLastRepoCommits')
        ->willReturn([$returnedCommit]);

        $manager = new GithubCommitsImporterManager(
            $this->clientAdapterMock,
            $this->receiverMock,
            $this->loggerMock
        );

        $this->assertTrue($manager->importLastCommits(1, 1, $this->symfonyStyleMock));
    }
}
