<?php

namespace App\Tests\Service\Github;

use PHPUnit\Framework\TestCase;
use App\Service\Github\GithubCommitMapper;

class GithubCommitMapperTest extends TestCase
{
    public function testMapCommitResponse()
    {
        $commitResponse = new \stdClass();
        $commitResponse->sha = '3fd04e789e569479de412b0c8a1265233bbb7115';
        $commitResponse->html_url = 'https://github.com/nodejs/node/commit/3fd04e789e569479de412b0c8a1265233bbb7115';
        $commitResponse->commit = new \stdClass();
        $commitResponse->commit->committer = new \stdClass();
        $commitResponse->commit->committer->name = 'Rich Trott';

        $githubCommitMapper = new GithubCommitMapper();
        $mappedCommit = $githubCommitMapper->mapCommitResponse($commitResponse);

        $this->assertIsArray($mappedCommit);
        $this->assertArrayHasKey('hash', $mappedCommit);
        $this->assertArrayHasKey('commiterName', $mappedCommit);
        $this->assertArrayHasKey('url', $mappedCommit);
        $this->assertSame([
            'hash' => '3fd04e789e569479de412b0c8a1265233bbb7115',
            'commiterName' => 'Rich Trott',
            'url' => 'https://github.com/nodejs/node/commit/3fd04e789e569479de412b0c8a1265233bbb7115'
        ], $mappedCommit);
    }
}
