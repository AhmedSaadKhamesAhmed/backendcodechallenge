<?php

namespace App\Tests\Command;

use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use App\Command\Github\GithubCommitsImporterCommand;
use App\Service\Github\GithubCommitsImporterManager;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class GithubCommitsImporterCommandTest extends TestCase
{
    private $managerMock;

    private $loggerMock;

    /**
     * @var CommandTester
     */
    private $commandTester;

    protected function setUp(): void
    {
        $this->managerMock = $this->createMock(GithubCommitsImporterManager::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);

        $application = new Application();
        $application->add(new GithubCommitsImporterCommand($this->managerMock, $this->loggerMock));
        $command = $application->find('app:github:commits:importer');
        $this->commandTester = new CommandTester($command);
    }

    protected function tearDown(): void
    {
        $this->managerMock = null;
        $this->loggerMock = null;
        $this->commandTester = null;
    }

    public function testExecuteSuccess(): void
    {
        $this->managerMock->expects($this->once())
            ->method('importLastCommits')
            ->willReturn(true);

        $this->assertEquals(1, $this->commandTester->execute(['--limit' => 25, '--per_chunk' => 25]));

        $this->assertStringContainsString("1/1 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%", $this->commandTester->getDisplay());
        $this->assertStringContainsString("successfully imported!", $this->commandTester->getDisplay());
    }
}
