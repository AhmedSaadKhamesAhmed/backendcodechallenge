<?php

namespace App\Tests\Entity;

use App\Entity\Commit;
use PHPUnit\Framework\TestCase;

class CommitTest extends TestCase
{
    /**
     * @dataProvider providerHashEndsInNumber
     */
    public function testHashEndsInNumber($hash, $expected)
    {
        $commit = new Commit();
        $commit->setHash($hash);

        $this->assertSame($commit->hashEndsInNumber(), $expected);
    }

    /**
     * @return array[]
     */
    public function providerHashEndsInNumber()
    {
        return [
            ['09d8c0c8d2c17fc07e9e313c0f35a7b696d18bc8', true],
            ['a5dec3a4707c8523529fc8446328ec8cf0011fd1', true],
            ['a518e4b871d39f0631beefc79cfa9dd81b82fe9f', false],
            ['4c06c40f4cab6a9ff438e6418fb96a61ef0673da', false],
        ];
    }
}
