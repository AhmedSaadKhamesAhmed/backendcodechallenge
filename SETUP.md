# Ahmed Saad / BackendCodeChallenge

### Requirments

* PHP >= 7.4
* Composer
* Docker
* Docker-compose
* Phpunit

### Installation guide

build and run the docker configuration 
```bash
docker-compose up -d
```

run composer
```bash
docker-compose exec php composer install
```

assign local .env file
```bash
docker-compose exec php cp .env.dist .env
```

add your github access token in .env file
```bash
GITHUB_TOKEN=here add you access token
```

create the database and it's migrations
```bash
docker-compose exec php php bin/console doctrine:database:create

docker-compose exec php php bin/console make:migration

docker-compose exec php php bin/console doctrine:migrations:migrate
```

### Run the commits importer command
```bash
docker-compose exec php php bin/console app:github:commits:importer --limit 25 --per_chunk 5
```

or without option
```bash
docker-compose exec php php bin/console app:github:commits:importer
```

### Access the web-ui in the browser
```bash
localhost:8887/commits
```

### Run the unit test

```bash
docker-compose exec php ./vendor/bin/phpunit
```


### Run CodeSniffer

To check your app code to accordance to PSR standards, run the following script

```bash
    docker-compose exec php php vendor/bin/phpcs --encoding=utf8
```

To fix your code according to PSR standards, run the following script

```bash
    docker-compose exec php php vendor/bin/phpcbf
```

### Note:
- as it's backend position, I used only bootstrap for the frontend for faster delivery
- forgot to remove my github accesstoken and pushed it but now it's removed from my account :)

### Answers to the questions: 

How were you debugging this mini-project? Which tools?
```bash
    # x-debug
```

How were you testing the mini-project?
```bash
    docker-compose exec php ./vendor/bin/phpunit
    
    # if I have more time I will right more unit test coverage and integration test
```

Imagine this mini-project needs microservices with one single database, how would you draft an architecture?
```bash
    # 4 microservices
    - Importer command line service
    - ApiGatway
    - Commits api (for getting the commits) with load balancer.
    - Web-UI service
    
    # 1 database
```
![alt text](microservices_diagram.jpg)

How would your solution differ when all over the sudden instead of saving to a Database you would have to call another external API to store and receive the commits.
```bash
    # using the concept of dependancy inversion I will just add and new ApiCallReceiver service which will implement the ReceiverInterface.php
    # and change the receiver wired class in the app/config/services.yaml
    
        App\Service\Github\GithubCommitsImporterManager:
        arguments:
            $clientAdapter: '@App\Service\Github\GithubClientAdapter'
            $receiver: '@App\Service\ApiCallReceiver'
```